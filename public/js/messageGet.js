const getMsg = (socket) => {
  socket.on("add msg", (data) => {
    $("#All_Msg").append(
      `
        <div class="d-flex flex-row justify-content-start">
            ${userName}
          <div>
            <p
              class="small p-2 ms-3 mb-1 rounded-3"
              style="background-color: #f5f6f7"
              id="myMsg"
            >
                ${data.msg}
            </p>
            <p class="small ms-3 mb-3 rounded-3 text-muted float-end">
            ${new Date().toLocaleTimeString()}
            </p>
          </div>
        </div>
    `
    );
  });
};

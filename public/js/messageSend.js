const socket = io();

jQuery(document).ready(() => {
  const socket = io.connect();
  jQuery("#msg-form").submit((e) => {
    e.preventDefault();
    $("form").serialize();
    const msg = jQuery("#msg");
    const id = jQuery("#id");
    console.log(msg, id);

    socket.emit("send msg", { id: id.val(), msg: msg.val() });
    msg.val("");
    $("#All_Msg").animate({ scrollTop: 9999 });
  });
  getMsg(socket);
});
const chatGet = (id) => {
  fetch(`/auth/chatGet/${id}`)
    .then((response) => {
      return response.json(); // Parse the response as JSON
    })
    .then((data) => {
      const userId = $("#id").val();
      data?.map((item) => {
        if (item?.sender == userId) {
          return $("#All_Msg").append(
            `
                        <div class="d-flex flex-row justify-content-start">
                          ${item?.sender_user}
                          <div>
                            <p
                              class="small p-2 ms-3 mb-1 rounded-3"
                              style="background-color: #f5f6f7"
                              id="myMsg"
                            >
                                ${item?.message}
                            </p>
                            <p class="small ms-3 mb-3 rounded-3 text-muted float-end">
                            ${new Date(item?.timestamp).toLocaleTimeString()}
                            </p>
                          </div>
                        </div>
                    `
          );
        } else {
          return $("#All_Msg").append(`
            <div class="d-flex flex-row justify-content-end">
                <div>
                    <p class="small p-2 me-3 mb-1 text-white rounded-3 bg-primary">
                        ${item?.message}
                    </p>
                    <p class="small me-3 mb-3 rounded-3 text-muted">${new Date(
                      item?.timestamp
                    ).toLocaleTimeString()}</p>
                </div>
                ${item?.sender_user}
            </div>
            `);
        }
      });
    })
    .catch((err) => console.log(err));
};
{
  /* <img
  src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-chat/ava6-bg.webp"
  alt="avatar 1"
  style="width: 45px; height: 100%"
/>; */
}

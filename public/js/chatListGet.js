const chatListGet = () => {
  const idVal = $("#id").val();
  fetch(`/auth/chatList/${idVal}`)
    .then((response) => {
      return response.json(); // Parse the response as JSON
    })
    .then((data) => {
      data?.map((item) =>
        $("#allUsers").append(`
        <li class="p-2 border-bottom">
        <a onclick="chatGet(${item.chat_id})" class="d-flex justify-content-between">
          <div class="d-flex flex-row">
            <div class="pt-1">
              <p class="fw-bold mb-0">${item.username}</p>
            </div>
          </div>
        </a>
      </li>
        `)
      );
    })
    .catch((err) => console.log(err));
};
chatListGet();

jQuery(document).ready(() => {
  jQuery("#searchForm").submit((e) => {
    e.preventDefault();
    $("form").serialize();
    const searchUser = jQuery("#searchFormUser");
    fetch(`/auth/searchUsers/${searchUser.val()}`)
      .then((response) => {
        return response.json(); // Parse the response as JSON
      })
      .then((data) => {
        data?.map((item) =>
          $("#allUsers").append(`
        <li class="p-2 border-bottom">
        <a href="/" class="d-flex justify-content-between">
          <div class="d-flex flex-row">
            <div class="pt-1">
              <p class="fw-bold mb-0">${item.username}</p>
              <p class="small text-muted">Hello, Are you there?</p>
            </div>
          </div>
        </a>
      </li>
        `)
        );
      })
      .catch((err) => console.log(err));
  });
});
console.log("hello");

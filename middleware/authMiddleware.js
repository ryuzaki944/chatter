const jwt = require("jsonwebtoken");

const requireAuth = (req, res, next) => {
  const token = req.cookies.userSave;
  if (!token) {
    res.render("login", { message: "Unauthorized" });
  } else {
    jwt.verify(token, process.env.JWT_SECRET, (err, decoded) => {
      if (err) {
        console.log(err, "err");
        res.render("login", { message: "Please login" });
      } else {
        console.log(decoded, "decoded");
        res.render("index", { id: decoded.id });
      }
    });
  }
};

module.exports = requireAuth;

const express = require("express");
const { createServer } = require("node:http");
const { join } = require("node:path");
const { Server } = require("socket.io");
const cookieParser = require("cookie-parser"); // Require the cookie-parser middleware
const index = require("./routes/index");
const auth = require("./controllers/auth");
const db = require("./models/db");

const port = 3000;
const app = express();
const server = createServer(app);
const io = new Server(server);

// STATIC FILES
app.use(express.static("public"));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.use("/js", express.static(__dirname + "public/js"));

// view engine setup
app.set("views", join(__dirname, "views"));
app.set("view engine", "ejs");

// Use the cookie-parser middleware
app.use(cookieParser());

app.use("/", index);
// app.use("/chat/:id", index);
app.use("/auth", auth);

const connections = [];

io.sockets.on("connection", (socket) => {
  console.log("a user connected");
  connections.push(socket);

  socket.on("disconnect", () => {
    console.log("a user disconnected");
    connections.splice(connections.indexOf(socket), 1);
  });
  socket.on("send msg", (data) => {
    console.log(data, "data");
    io.sockets.emit("add msg", {
      msg: data.msg,
    });
  });
});

server.listen(port, () => {
  console.log(`server running at http://localhost:${port}`);
});

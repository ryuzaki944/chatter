const connection = require("../models/db");

// SQL query to create a table for messages
const createChatTableQuery = `
  CREATE TABLE IF NOT EXISTS chat_messages (
    id INT AUTO_INCREMENT PRIMARY KEY,
    sender VARCHAR(255) NOT NULL,
    receiver VARCHAR(255) NOT NULL,
    message TEXT NOT NULL,
    chat_id INT NOT NULL,
    timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (chat_id) REFERENCES chat(id)
  )
`;

connection.query(createChatTableQuery, (err, results) => {
  if (err) {
    console.error("Error creating messages table:", err);
  } else {
    console.log("Messages table created successfully");
  }
});

// Close the MySQL connection when done
connection.end();

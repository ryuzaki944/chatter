const connection = require("../models/db");

// SQL query to create a table for chat
const createChatTableQuery = `
  CREATE TABLE IF NOT EXISTS chat (
    id INT AUTO_INCREMENT PRIMARY KEY,
    sender VARCHAR(255) NOT NULL,
    receiver VARCHAR(255) NOT NULL,
    messages_count INT,
    timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP
  )
`;

connection.query(createChatTableQuery, (err, results) => {
  if (err) {
    console.error("Error creating messages table:", err);
  } else {
    console.log("Messages table created successfully");
  }
});

// Close the MySQL connection when done
connection.end();

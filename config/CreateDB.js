const mysql = require("mysql2");

const connection = mysql.createConnection({
  host: "localhost", // Replace with your MySQL server host
  user: "root", // Replace with your MySQL username
  password: "", // Replace with your MySQL password
});

const createDatabaseQuery = "CREATE DATABASE IF NOT EXISTS chatt";

connection.query(createDatabaseQuery, (err, results) => {
  if (err) {
    console.error("Error creating database:", err);
  } else {
    console.log("Database created successfully");
  }
});
// Close the MySQL connection when done
connection.end();
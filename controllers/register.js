const db = require("../models/db");
const bcrypt = require("bcryptjs/dist/bcrypt");

exports.register = (req, res) => {
  const { login, password } = req?.body;

  db.query(
    `SELECT * FROM users WHERE username = ?`,
    [login],
    async (error, result, fields) => {
      if (error) throw error;

      if (result.length > 0) {
        res.render("register", { message: "Login is already exist" });
      } else {
        const hashedPassword = await bcrypt.hash(password, 8);

        db.query(
          `INSERT INTO users SET ?`,
          {
            username: login,
            password: hashedPassword,
          },
          (err, resu, fiel) => {
            if (err) throw err;

            res.render("register", {
              message: `New login(${login}) were created!`,
            });
          }
        );
      }
    }
  );
};

const mysql = require("mysql2/promise"); // Use mysql2/promise
const dotenv = require("dotenv");

dotenv.config({ path: "../.env" }); // MySQL configuration
const dbConfig = {
  host: "localhost", // Replace with your MySQL server host
  user: process.env.DATABASE_USER, // Replace with your MySQL username
  password: "", // Replace with your MySQL password
  database: process.env.DATABASE, // Replace with your MySQL database name
  port: 3306,
};

// Create a connection pool with promise support
const pool = mysql.createPool(dbConfig);

exports.chatList = async (req, res) => {
  try {
    const id = req.params.id;
    const db = await pool.getConnection();
    // GET CHAT
    const [rows] = await db.query(
      "SELECT * FROM chat, chat_messages WHERE chat.id = ?",
      [id]
    );
    db.release();

    if (rows.length === 0) {
      res.status(404).json({ error: "No chats" });
    } else {
      const user1 = await db.query(
        "SELECT id, username FROM users WHERE id = ?",
        rows[0].sender
      );
      const user2 = await db.query(
        "SELECT id, username FROM users WHERE id = ?",
        rows[0].receiver
      );
      const data = rows?.map((item) => {
        const obj = {
          ...item,
          sender_user:
            user1[0][0].id == item.sender
              ? user1[0][0].username
              : user2[0][0].username,
        };
        return obj;
      });
      res.status(200).json(data);
    }
  } catch (err) {
    res.status(500).json({ error: "Internal Server Error" });
  }
};

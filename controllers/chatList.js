const mysql = require("mysql2/promise"); // Use mysql2/promise
const dotenv = require("dotenv");

dotenv.config({ path: "../.env" }); // MySQL configuration
const dbConfig = {
  host: "localhost", // Replace with your MySQL server host
  user: process.env.DATABASE_USER, // Replace with your MySQL username
  password: "", // Replace with your MySQL password
  database: process.env.DATABASE, // Replace with your MySQL database name
  port: 3306,
};

// Create a connection pool with promise support
const pool = mysql.createPool(dbConfig);

exports.chatList = async (req, res) => {
  try {
    const id = req.params.id;
    const db = await pool.getConnection();

    const user1 = await db.query(
      `SELECT users.id, users.username, chat.id AS chat_id, chat.sender, chat.receiver, chat.messages_count FROM users INNER JOIN chat ON chat.sender = users.id WHERE receiver = '${id}'`,
      [id]
    );
    const user2 = await db.query(
      `SELECT users.id, users.username, chat.id AS chat_id, chat.sender, chat.receiver, chat.messages_count FROM users INNER JOIN chat ON chat.receiver = users.id WHERE sender = '${id}'`,
      [id]
    );
    const data = user1[0].concat(...user2[0]);
    console.log(data);
    db.release();
    if (data.length === 0) {
      res.status(404).json({ error: "No chats" });
    } else {
      res.status(200).json(data);
    }
  } catch (err) {
    res.status(500).json({ error: "Internal Server Error" });
  }
};

const db = require("../models/db");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs/dist/bcrypt");
require("dotenv").config("../.env");

exports.login = (req, res) => {
  try {
    const { login, password } = req?.body;
    db.query(
      "SELECT * FROM users WHERE username = ?",
      [login],
      async (err, results) => {
        console.log(results, login, password);
        if (
          !results ||
          !(await bcrypt.compare(password, results[0].password))
        ) {
          res.render("login", { message: "Incorrect data" });
        } else {
          const id = results[0].id;

          const token = jwt.sign({ id }, process.env.JWT_SECRET, {
            expiresIn: process.env.JWT_EXPIRES,
          });

          const cookieOptions = {
            expires: new Date(
              Date.now() + process.env.JWT_COOKIE_EXPIRES * 24 * 60 * 60 * 1000
            ),
            httpOnly: true,
          };
          res.cookie("userSave", token, cookieOptions);
          res.status(200).redirect("/");
        }
      }
    );
  } catch (err) {
    console.log(err);
  }
};

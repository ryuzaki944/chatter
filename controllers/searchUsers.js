const mysql = require("mysql2/promise"); // Use mysql2/promise
const dotenv = require("dotenv");

dotenv.config({ path: "../.env" }); // MySQL configuration
const dbConfig = {
  host: "localhost", // Replace with your MySQL server host
  user: process.env.DATABASE_USER, // Replace with your MySQL username
  password: "", // Replace with your MySQL password
  database: process.env.DATABASE, // Replace with your MySQL database name
  port: 3306,
};

// Create a connection pool with promise support
const pool = mysql.createPool(dbConfig);

exports.searchUsers = async (req, res) => {
  try {
    const user = req.params.user;
    const db = await pool.getConnection();

    const [rows] = await db.query(
      "SELECT username FROM users WHERE username = ?",
      [user]
    );
    console.log(rows);
    db.release();
    if (rows.length === 0) {
      res.status(404).json({ error: "User not found" });
    } else {
      res.status(200).json(rows);
    }
  } catch (err) {
    res.status(500).json({ error: "Internal Server Error" });
  }
};

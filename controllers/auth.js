const express = require("express");
const authControllerReg = require("./register");
const authControllerLog = require("./login");
const authControllerOut = require("./logout");
const authControllerSearch = require("./searchUsers");
const authControllerChats = require("./chatList");
const authControllerChat = require("./chatGet");
const router = express.Router();

router.post("/register", authControllerReg.register);
router.post("/login", authControllerLog.login);
router.get("/searchUsers/:user", authControllerSearch.searchUsers);
router.get("/chatList/:id", authControllerChats.chatList);
router.get("/chatGet/:id", authControllerChat.chatList);
router.get("/logout", authControllerOut.logout);

module.exports = router;

const mysql = require("mysql2");
const dotenv = require("dotenv");

dotenv.config({ path: "./.env" });

// Create a connection to the MySQL server
const connection = mysql.createConnection({
  host: "localhost", // Replace with your MySQL server host
  user: process.env.DATABASE_USER, // Replace with your MySQL username
  password: "", // Replace with your MySQL password
  database: process.env.DATABASE, // Replace with your MySQL database name
  port: 3306
});

// Connect to the MySQL server
connection.connect((err) => {
  if (err) {
    console.error("Error connecting to MySQL:", err);
    return;
  }
  console.log("Connected to MySQL database");
});

// Perform database operations here

module.exports = connection;
// // Close the MySQL connection when done
// connection.end();

const express = require("express");
const authController = require("../middleware/authMiddleware");
const router = express.Router();

/* GET home page. */
router.get("/", authController, function (req, res, next) {
  res.render("index", { id: "" });
});

router.get("/login", function (req, res, next) {
  res.render("login", { message: "" });
});

router.get("/register", function (req, res, next) {
  res.render("register", { message: "" });
});

module.exports = router;
